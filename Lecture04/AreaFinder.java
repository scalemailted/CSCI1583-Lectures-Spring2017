import java.util.Scanner;

public class AreaFinder
{
    public static void main(String[] args)
    {
    
        /*Setup Data*/
        //create a scanner object
        Scanner keyboard = new Scanner(System.in);
        //create a length variable
        double length;
        //create a width variable
        double width;
        
        /*Algorithm for calculating area*/
        //prompt the user for the length
        System.out.print("Enter length: ");
        //get the input and save to variable
        length = keyboard.nextDouble();
        //prompt the user for the width
        System.out.print("Enter width: ");
        //get the input and save to the variable
        width = keyboard.nextDouble();
        //multiply the width by the length
        double area = length * width;
        //display the answer to the user.
        System.out.printf("The area is %.2f\n", area);
    }
    
}