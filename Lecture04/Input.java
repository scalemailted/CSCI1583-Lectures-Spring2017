import java.util.Scanner;

public class Input
{
    public static void main(String[] args)
    {
       //create a new scanner object
       Scanner input = new Scanner(System.in);
       
       /*
       //prompt the user
       System.out.print("Enter your first & last name: ");
       
       //get user input
       String first = input.next();
       
       //print the user input
       System.out.printf( "First Name: %s\n", first );
       
       String last = input.next();
       //print the user input
       System.out.printf( "Last Name: %s\n", last );
       */
       
       System.out.print( "Enter a number: ");
       int choice = input.nextInt();
       System.out.printf( "the choice: %d\n", choice *100);
    }
}