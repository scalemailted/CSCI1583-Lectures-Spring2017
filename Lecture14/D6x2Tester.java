public class D6x2Tester
{
    public static void main(String[] args)
    {
        int[] frequency = new int[13];
        for (int i=0; i<11000000;i++)
        {
            int diceSum = Dice.rollD6(2);
            frequency[diceSum]++; 
        }
        
        for(int diceFace=2; diceFace < frequency.length; diceFace++)
        {
            System.out.printf("%d: %d\n",diceFace, frequency[diceFace]);
        }
    }
}