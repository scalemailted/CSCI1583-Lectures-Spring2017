public class EnhancedFor
{
    public static void main(String[] args)
    {
        String[] namesArray = {"Tom", "Mary", "Sue", "John"};
        /*
        for (int i=0; i<namesArray.length; i++)
        {
            System.out.printf("%d: %s\n",i,namesArray[i]);
            namesArray[i] = "Ted"+i;
        }
        
        for (int i=0; i<namesArray.length; i++)
        {
            System.out.printf("%d: %s\n",i,namesArray[i]);
        }*/
        
        for(String name : namesArray)
        {
            System.out.println(name);
        }
    }
}