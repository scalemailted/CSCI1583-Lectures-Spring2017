import java.util.Random;

public class Monster
{
    public static Random rand = new Random();
    
    public static int health;
    public static int attackPower;
    
    public static void takeDamage(int damage)
    {
        health -= damage;
    }
    
    public static void printStats()
    {
        System.out.printf("Monster - HP:%d, AP%d\n",health,attackPower);
    }
    
    public static void attack()
    {
        if (Hero.isFighting == true)
        {
            int damage = rand.nextInt(attackPower) + 1;
            Hero.takeDamage(damage);
            System.out.printf("Monster attacked you for %d damage\n", damage);
        }
    }
    
    public static void createMonster()
    {
        health = 75;
        attackPower = 10;
    }
}