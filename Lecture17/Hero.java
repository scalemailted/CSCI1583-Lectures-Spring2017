import java.util.Random;
import java.util.Scanner;

public class Hero
{
    public static Random rand = new Random();
    public static Scanner input = new Scanner(System.in);
    
    public static int health;
    public static int attackPower;
    public static int magicPower;
    public static boolean isFighting = true;
    
    public static void takeDamage(int damage)
    {
        health -= damage;
    }
    
    public static void swordAttack()
    {
        int damage = rand.nextInt(attackPower) + 1;
        Monster.takeDamage(damage);
        System.out.printf("You attacked for %d damage\n", damage);
    }
    public static void magicAttack()
    {
        if (magicPower >= 3)
        {
            System.out.println("You cast a spell");
            Monster.takeDamage(Monster.health/2);
        }
    }
    public static void manaCharge()
    {
        System.out.println("You charge up your mana");
        magicPower++;
    }
    public static void runAway()
    {
        isFighting = false;
    }
    
    public static void takeTurn()
    {
        System.out.println("Choose an action:");
        System.out.println("1) Sword Attack");
        System.out.println("2) Magic Attack");
        System.out.println("3) Mana Charge");
        System.out.println("4) Flee");
        int choice = input.nextInt();
        switch(choice)
        {
            case 1: swordAttack(); break;
            case 2: magicAttack(); break;
            case 3: manaCharge(); break;
            case 4: runAway(); break;
            default: System.out.println("Not a valid choice");
        }
        
    }
    
    public static void printStats()
    {
        System.out.printf("Hero - HP:%d, AP:%d, MP:%d\n",health,attackPower,magicPower);
    }
    
    public static void createHero()
    {
        health = 100;
        attackPower = 8;
        magicPower = 6;
    }
    
}