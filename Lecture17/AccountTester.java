import java.util.Scanner;

public class AccountTester
{
    public static void main(String[] args)
    {
        //create scanner
        Scanner input = new Scanner(System.in);
        //create some accounts
        Account account1 = new Account();
        Account account2 = new Account();
        
        //display the initial state of account variables
        System.out.println(account1);
        System.out.println(account2);
        
        //prompt for name in account 1
        System.out.print("Enter first and last name: ");
        String first = input.next();
        String last = input.next();
        
        //Set name into the account
        account1.setFirstName(first);
        account1.setLastName(last);
        
        //prompt for name in account 2
        System.out.print("Enter first and last name: ");
        first = input.next();
        last = input.next();
        
        //Set name into the account
        account2.setFirstName(first);
        account2.setLastName(last);
        
        //display the two accounts
        System.out.println(account1);
        System.out.println(account2);
    }
}