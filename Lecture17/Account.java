public class Account
{
    //account properties
    private String firstName;
    private String lastName;
    private int id;
    private double balance;
    
    //constructor
    public Account()
    {
        
    }

    //account methods
    /*getters*/
    public String getFirstName() { return this.firstName; }
    public String getLastName()  { return this.lastName; }
    public int getID()           { return this.id; }
    public double getBalance()   { return this.balance; }
    
    /*setters*/
    public void setFirstName(String first) 
    {
        this.firstName = first;
    }
    
    public void setLastName(String last) 
    {
        this.lastName = last;
    }
    
    /*
    public String toString()
    {
        return String.format("First: %s, Last: %s", 
                             this.firstName, 
                             this.lastName);
    }*/
    
    //manage assign an id
    //deposit money
    //withdraw money
}