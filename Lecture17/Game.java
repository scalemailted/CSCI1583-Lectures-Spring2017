public class Game
{
    public static void main(String[] args)
    {
        Hero.createHero();
        Monster.createMonster();
        
        while (Hero.health > 0 && Monster.health > 0 && Hero.isFighting)
        {
            Hero.printStats();
            Monster.printStats();
            Hero.takeTurn();
            Monster.attack();
        }
    }
}