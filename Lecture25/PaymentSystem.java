public class PaymentSystem
{
    public static void main(String[] args)
    {
        Employee kim = new SalaryEmployee("Kim", "Kimmie", new Date(9,9,1999), 5000);
        Employee jim = new HourlyEmployee("Jim", "Gymmie", new Date(12,12,2012), 15.00, 20);
        Employee tim = new HourlyEmployee("Tim", "Timm", new Date(14,1,2016), 25.00, 5);
        Employee ryan = new CommissionEmployee("Ryan", "Riann", new Date(31,10,2009), 0.25, 50000);
        
        Employee[] employees = {kim, jim, tim, ryan};
        
        double sum = 0;
        
        for (Employee i : employees)
        {
            System.out.println(i);
            System.out.println(i.getEarnings());
            sum += i.getEarnings();
        }
        System.out.println(sum);
    }
}