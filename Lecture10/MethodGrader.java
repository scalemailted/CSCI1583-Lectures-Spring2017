import java.util.Scanner;

public class MethodGrader
{
    public static void main(String[] args)
    {
        // Get Lab grades from user and sum them.
        double testAvg = getAverage("Test");
        // Get Homework grades from user and sum them.
        double hwAvg = getAverage("Homework");
        // Get Test grades from user and sum them.
        double labAvg = getAverage("Lab");
        
        // Calculate the average and print it.
        printFinalGrade(testAvg, hwAvg, labAvg);
    }


    public static double getAverage(String gradeType)
    {
        Scanner input = new Scanner(System.in);
        double sum = 0;
        int count = 0;
        
        System.out.printf("Enter your %s grades or -1 to exit:\n", gradeType);
        double grade = input.nextDouble();
        
        while (grade >= 0)
        {
            sum += grade;
            count++;
            grade = input.nextDouble();
        }
        
        double average = sum/count;
        System.out.printf("%s average: %.2f\n",gradeType, average);
        return average;
    }
    
    public static void printFinalGrade(double tests, double hws, double labs)
    {
        double finalGrade = tests*0.4 + hws*0.5 + labs*0.1;
        System.out.printf("Final grade: %f\n",finalGrade);
    }
    
}