/* Top:
* Determine a student's average for 
* any number of Labs, Homework, and Test.
*/
import java.util.Scanner;

public class MethodGrader2
{
    /*main algorithm*/
    public static void main(String[] args)
    {
        // Get Lab grades from user and average them.
        double labAverage = getAverage("Lab");
        // Get Homework grades from user and average them.
        double hwAverage = getAverage("Homework");
        // Get Test grades from user and average them.
        double testAverage = getAverage("Test");
        // Calculate the final grade and print it.
        printFinalGrade(testAverage, hwAverage, labAverage);
    }

    // (Method - Task: Getting average)
    public static double getAverage( String gradeType)
    {
        //initialize data for averaging
        Scanner input = new Scanner(System.in);
        int count = 0;
        double sum = 0;
        
        //Prompt the user for input
        System.out.printf("Enter your %s grades or -1 to stop:\n", gradeType);
        //Get that input and save it
        double grade = input.nextDouble();
        
        // repeat until the grade is less than 0
        while (grade >= 0)
        {
            //add grade to the sum
            sum += grade;
            //add one to the count 
            count++;
            //Get and store the grade from the user
            grade = input.nextDouble();
        }
        
        //calculate the average
        double average = sum / count;
        System.out.printf("%s average: %.2f\n", gradeType, average);
        return average;
    }


    // (Method: Task: calculating the average)
    public static void printFinalGrade(double tests, double hws, double labs)
    {
        // calculate the final grade
        double finalGrade = tests*0.4 + hws*0.5 + labs*0.1;
        // Print average
        System.out.printf("Final grade: %f\n", finalGrade);
    }


}