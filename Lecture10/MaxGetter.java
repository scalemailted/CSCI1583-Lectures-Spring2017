public class MaxGetter
{
    public static double PI = 3.14159;
    
    public static double max(double a, double b, double c)
    {
        double maxValue = a;
        if (b > maxValue)
        {
            maxValue = b;
        }
        if (c > maxValue)
        {
            maxValue = c;
        }
        return maxValue;
    }
}