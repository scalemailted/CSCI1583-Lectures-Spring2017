public class MethodTest1
{
    public static void main(String[] args)
    {
        //printWords10x();
        //printWords(5);
        //printWords(3,"Hello World!");
        //int five = fiveMaker();
        //System.out.printf("%d\n",five);
        int quarters = quarterBack(1.25);
        System.out.printf("There are %d quarters in $%.2f\n", quarters, 1.25);
    }
    
    //public static outputDataType methodName(inputDataType variableName)
    public static void printWords10x()
    {
        for (int i=0; i<10; i++)
        {
            System.out.println("These are words!");
        }
    }
    
    public static void printWords(int numberOfTimes)
    {
        for (int i=0; i<numberOfTimes; i++)
        {
            System.out.println("These are words!");
        }
    }
    
    public static void printWords(int numberOfTimes, String words)
    {
        for (int i=0; i<numberOfTimes; i++)
        {
            System.out.println(words);
        }
    }
    
    public static int fiveMaker()
    {
        System.out.println("Here is your 5!");
        return 5;
    }
    
    public static int quarterBack(double dollars)
    {
        return (int)(dollars/0.25);
    }
    
    public static void printWords5x()
    {
        for (int i=0; i<fiveMaker(); i++)
        {
            System.out.println("These are words!");
        }
    }
    
    
}