public class X
{
    private int x;
    
    public X()
    {
        this.x = 0;
    }
    
    public void fun()
    {
        System.out.println("Hello world");
    }
    
    public void fun(int i)
    {
        System.out.println("Hello i");
    }
    
    public String toString()
    {
        return "X";
    }
}