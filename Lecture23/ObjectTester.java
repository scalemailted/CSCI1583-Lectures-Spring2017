public class ObjectTester
{
    public static void main(String[] args)
    {
        X x = new X();
        Y y = new Y();
        Object[] things = {x, y, 32, 1.45, true, "Hello", 'g'};
        for (Object item : things)
        {
            System.out.println(item);
        }
    }
}