public class Point2d
{
    private int x;
    private int y;
    
    public Point2d(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public int getX()
    {
        return this.x;
    }
    
    public int getY()
    {
        return this.y;
    }
    
    public void setX(int x)
    {
        this.x = x;
    }
    
    public void setY(int y)
    {
        this.y = y;
    }
    
    public double distance(Point2d otherPoint)
    {
        int x1 = this.x;
        int x2 = otherPoint.getX();
        int y1 = this.y;
        int y2 = otherPoint.getY();
        int dx = (x2-x1) * (x2-x1);
        int dy = (y2-y1) * (y2-y1);
        return Math.sqrt(dx+dy);
    }
}