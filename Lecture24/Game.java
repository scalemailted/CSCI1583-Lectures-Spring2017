public class Game
{
    public static void main(String[] args)
    {
        Hero hero = new Hero();
        Monster monster = new Monster();
        while (true)
        {
            hero.meleeAttack(monster);
            monster.attack(hero);
        }
    }
}