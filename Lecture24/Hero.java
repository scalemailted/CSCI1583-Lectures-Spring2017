public class Hero extends GameCharacter
{
    private int mp;
    private int lvl;
    
    public Hero()
    {
        super(100, 10);
        this.mp = 6;
        this.lvl = 1;
    }
    
    public void meleeAttack(Monster monster)
    {
        System.out.printf("You hit monster for %d damage\n", this.getAP());
        monster.takeDamage(this.getAP());
    }
    
    
    
}