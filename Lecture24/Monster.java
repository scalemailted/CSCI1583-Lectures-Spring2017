public class Monster extends GameCharacter
{
    private int xp;
    
    public Monster()
    {
        super(80, 8);
        this.xp = 25;
    }
    
    public void attack(Hero hero)
    {
        System.out.printf("The monster attacks you for %d damage\n",this.getAP());
        hero.takeDamage(this.getAP() );
    }
}