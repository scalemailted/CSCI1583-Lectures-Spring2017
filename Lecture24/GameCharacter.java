public class GameCharacter
{
    private int hp;
    private int ap;
    
    public GameCharacter(int hp, int ap)
    {
        this.hp = hp;
        this.ap = ap;
    }
    
    public void takeDamage(int damage)
    {
        this.hp = this.hp - damage;
    }
    
    public int getHP()
    {
        return this.hp;
    }
    
    public int getAP()
    {
        return this.ap;
    }
}