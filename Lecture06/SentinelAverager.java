import java.util.Scanner;

public class SentinelAverager
{

    public static void main(String[] args)
    {
        // Initialize sum to 0
        double sum = 0;
        // Initialize count to 0
        int count = 0;
        // Initialize Scanner object for input
        Scanner input =  new Scanner(System.in);
        // declare an average
        double average;
        // declare a grade
        double grade;
        
        // Prompt the user for a grade
        System.out.print("Enter a grade or -1 to stop: ");
        // Get and store the grade from the user
        grade = input.nextDouble();
        
        // repeat until the grade is less than 0
        while (grade > 0)
        {
            // add grade to the sum
            sum = sum + grade;
            // add one to the count 
            count = count + 1;
            System.out.print("Enter a grade or -1 to stop: ");
            // Get and store the grade from the user
            grade = input.nextDouble();
        }
        
        if (count > 0)
        {
            // Set the average to the sum / count
            average = sum / count;
            // Print average
            System.out.printf("Average: %f\n", average);
        }
    }
}














