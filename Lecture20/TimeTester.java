public class TimeTester
{
    public static void main(String[] args)
    {
        Time t1 = new Time(2,3,4);
        Time t2;
        try
        {
            t2 = new Time(12290121,31,41);
        }
        catch (IllegalArgumentException e)
        {
            t2 = new Time(13);
        }
        System.out.printf("t1: %s\n", t1.getUniversal());
        System.out.printf("t1: %s\n", t1);
        System.out.printf("t2: %s\n", t2.getUniversal());
        System.out.printf("t2: %s\n", t2);
        //t2.setTime(12,12,1212);
        
    }
}