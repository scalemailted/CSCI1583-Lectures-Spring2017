public class DateTester
{
    public static void main(String[] args)
    {
        Date d1 = new Date(25, 12, 1999);
        System.out.println(d1);
        
        Date d2 = new Date(25, 120, 1999);
        System.out.println(d2);
    }
}