import java.util.Random;

public class DiceTester
{
    public static void main(String[] args)
    {
        
        Random randomGenerator = new Random();
        randomGenerator.setSeed(0);
        for (int i=0; i<10; i++)
        {
            int randomInt = randomGenerator.nextInt(6)+1;
            System.out.printf("%d\n",randomInt);
        }
    }
}