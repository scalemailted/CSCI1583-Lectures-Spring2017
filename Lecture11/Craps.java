// Top: the Game of Craps

public class Craps
{
    // setup data:
    private enum Status {CONTINUE, LOST, WIN};
    
    // special values for win conditions
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int  = 11;
    private static final int BOXCAR = 12;
    
    // game status
    private static Status gameStatus;
    // myPoint
    private static int myPoint;
    
    // main:
    public static void main(String[] args)
    {
        // Setup data
        // play first round 
        // if no winner then conintue playing next rounds
        // print game results
    }
    
    public static void firstRound()
    {
        // first Round:
        //     roll dice
        //     update games status if we win or lose
        //     if neither set myPoint
    }
            
    public static void nextRound()
    {
        // while game status is continue:    
        // next Round:
        //     roll dice
        //     if roll is myPoint we win and update the game status
        //     if roll is seven we lose and update the game status
    }
    
    public static void printResults()
    {
         // print Results:
        //     if we win print a victory message
        //     if we lost print a defeat message
    }
}