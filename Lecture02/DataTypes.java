public class DataTypes
{
    public static void main(String[] args)
    {
        int x = 4;
        //storage operation of int
        int integerVariable;
        integerVariable = x - 7;
        
        //print that int
        System.out.printf("The int value: %d\n",integerVariable);
        
        //storage operation of float
        double floatVariable = 3.14;
        //print that float
        System.out.printf("The float value: %.2f\n",floatVariable);
        
        //storage operation of boolean
        boolean tf = true; 
        //print that boolean
        System.out.printf("The boolean value is %b\n", tf);
        
        //storage operation of String
        String textData;
        textData = "123445";
        //print that String
        System.out.printf("Text: %s\n", textData);
        
        //storage operation of char
        char character;
        character = '1';
        //print that char
        System.out.printf("character: %c\n", character);
    }
}