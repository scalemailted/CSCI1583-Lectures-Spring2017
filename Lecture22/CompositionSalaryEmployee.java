public class SalaryEmployee
{
    private double salary;
    private Employee employee;
    
    public SalaryEmployee(String first, String last, Date hire, double salary)
    {
        this.employee = new Employee(first, last, hire);
        this. salary = salary;
    }
    
    public String getFirstName()
    {
        return employee.getFirstName();
    }
}