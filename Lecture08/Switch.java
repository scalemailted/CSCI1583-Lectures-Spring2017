import java.util.Scanner;

public class Switch
{
    public static void main(String[] args)
    {
        // declare variable for lefthandOperand
        int lefthand;
        // declare variable for righthandOperand
        int righthand;
        // declare variable for operator
        String operator;
        // declare variable for result
        int result = 0;
        // setup scanner object for input
        Scanner input = new Scanner(System.in);
        
        // prompt user for math expression
        System.out.println("Enter a mathematical expression separated by spaces: ");
        // get lefthandOperand from user
        lefthand = input.nextInt();
        // get operator from user
        operator = input.next();
        // get righthandOperand from user
        righthand = input.nextInt();
        
        // multiselection statement where:
        switch(operator)
        {
            // if operator is a + we add 
            case "+": 
                {
                    result = lefthand + righthand;
                    System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
                }
                break;
            // if operator is a - we subtract
            case "-": 
                {
                    result = lefthand - righthand;
                    System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
                }
                break;
            // if operator is a * we multiply
            case "x":
            case "X":
            case "*": 
                {
                    result = lefthand * righthand;
                    System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
                }
                break;
            // if operator is a / we divide
            case "/": 
                {
                    result = lefthand / righthand;
                    System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
                }
                break;
            default: 
            {
                System.out.println("Not a operator!");
            }
        }
        
        // print the result
        
        
        
    }
}