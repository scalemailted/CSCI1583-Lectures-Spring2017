import java.util.Scanner;

public class Switch2If
{
    public static void main(String[] args)
    {
        // declare variable for lefthandOperand
        int lefthand;
        // declare variable for righthandOperand
        int righthand;
        // declare variable for operator
        String operator;
        // declare variable for result
        int result = 0;
        // setup scanner object for input
        Scanner input = new Scanner(System.in);
        
        // prompt user for math expression
        System.out.println("Enter a mathematical expression separated by spaces: ");
        // get lefthandOperand from user
        lefthand = input.nextInt();
        // get operator from user
        operator = input.next();
        // get righthandOperand from user
        righthand = input.nextInt();
        
        // multiselection statement where:
        // if operator is a + we add 
        if (operator == "+") 
        {
            result = lefthand + righthand;
            System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
        }
        // if operator is a - we subtract
        else if (operator == "-")
        {
            result = lefthand - righthand;
            System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
        }
        // if operator is a * we multiply
        else if (operator == "*") 
        {
            result = lefthand * righthand;
            System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
        }
        // if operator is a / we divide
        else if (operator == "/") 
        {
            result = lefthand / righthand;
            System.out.printf("%d%s%d=%d\n",lefthand, operator, righthand, result);
        }
        else 
        {
        System.out.println("Not a operator!");
        }
        
    }
}