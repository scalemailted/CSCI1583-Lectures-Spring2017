public class InitArray2
{
    public static void main(String[] args)
    {
        //init array
        String[] groceryArray = {"eggs", "milk", "bread", "bread", "rice", "noodle"};
        
        for (int i=0; i< groceryArray.length; i++)
        {
            String item = groceryArray[i];
            System.out.println(item);
        }
        
    }
}