// Top: the Game of Craps

public class Craps
{
    // setup data:
    private enum Status {CONTINUE, LOST, WIN};
    
    // special values for win conditions
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int YO = 11;
    private static final int BOXCAR = 12;
    
    // game status
    private static Status gameStatus;
    // myPoint
    private static int myPoint;
    
    // main:
    public static void main(String[] args)
    {
        // play first round 
        firstRound();
        // if no winner then conintue playing next rounds
        while (gameStatus == Status.CONTINUE)
        {
            nextRound();
        }
        // print game results
        printResults();
    }
    
    // first Round:
    public static void firstRound()
    {
        // roll dice
        int diceSum = Dice.rollD6(2);
        // update games status if we win (7,11) 
        if (diceSum == SEVEN || diceSum == YO)
        {
            gameStatus = Status.WIN;
        }
        // else if we lose update games status (2,3,12) 
        else if (diceSum == SNAKE_EYES
                 || diceSum == TREY
                 || diceSum == BOXCAR)
        {
            gameStatus = Status.LOST;
        }
        // else if neither set myPoint
        else
        {
            System.out.printf("Your point is %d\n",diceSum);
            myPoint = diceSum;
            gameStatus = Status.CONTINUE;
        }
    }
        
    // next Round:    
    public static void nextRound()
    {
        // roll dice
        int diceSum = Dice.rollD6(2);
        // if roll is myPoint we win and update the game status
        if (diceSum == myPoint)
        {
            gameStatus = Status.WIN;
        }
        // if roll is seven we lose and update the game status
        else if (diceSum == SEVEN)
        {
            gameStatus = Status.LOST;
        }
    }
    
    // print Results:
    public static void printResults()
    {
        //     if we win print a victory message
        if (gameStatus == Status.WIN)
        {
            System.out.println("You won!");
        }
        //     if we lost print a defeat message
        else
        {
            System.out.println("You lost!");
        }
    }
}