import java.util.Random;

public class Dice
{
    private static Random randomGenerator = new Random();
    
    public static int rollD6()
    {
        int die = randomGenerator.nextInt(6) + 1;
        return die;
    }
    
    public static int rollD6(int quantity)
    {
        String description = "";
        int dieSum = 0;
        for (int i =0; i< quantity; i++)
        {
            
            int die = randomGenerator.nextInt(6) + 1;
            dieSum += die;
            description += (i==quantity-1) ? die+"=" : die+"+";
        }
        description += dieSum;
        System.out.println(description);
        return dieSum;
    }
}