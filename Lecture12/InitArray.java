public class InitArray
{
    public static void main(String[] args)
    {
        //init array
        String[] groceryArray = new String[6];
        groceryArray[0] = "eggs";
        groceryArray[1] = "milk";
        groceryArray[2] = "bread";
        groceryArray[3] = "bread";
        groceryArray[4] = "rice";
        groceryArray[5] = "noodles";
        
        for (int i=0; i< groceryArray.length; i++)
        {
            String item = groceryArray[i];
            System.out.println(item);
        }
        
    }
}