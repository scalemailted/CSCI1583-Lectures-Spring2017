import java.util.Scanner;

public class AccountTester2
{
    public static void main(String[] args)
    {
        //create some accounts
        Account account1 = new Account("Ted", "Holmberg");
        Account account2 = new Account("Tom", "Harrison");
        
        //display the initial state of account variables
        System.out.println(account1);
        System.out.println(account2);
        account1.setFirstName("Kyle");
        System.out.println(account1);
        
    }
}