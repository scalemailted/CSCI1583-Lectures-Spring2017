import java.util.Scanner;

public class AccountTester3
{
    public static void main(String[] args)
    {
        //create some accounts
        Account account1 = new Account("Ted", "Holmberg");
        Account account2 = new Account("Tom", "Harrison");
        
        //display the initial state of account variables
        System.out.println(account1);
        System.out.println(account2);

        account1.deposit(10.00);
        account2.deposit(-20.00);
        
        System.out.println(account1);
        System.out.println(account2);
        
        account1.withdraw(5.00);
        account2.withdraw(20000.00);
        
        System.out.println(account1);
        System.out.println(account2);
        
        Account account3 = new Account("Kiki", "Harty");
        Account account4 = new Account("Stella", "Dimitri");
        
        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);
        System.out.println(account4);
        System.out.println(Account.getCount());
        System.out.println(account1.getCount());
    }
}