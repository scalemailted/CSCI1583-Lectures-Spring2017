public class Account
{
    //class properties
    private static int numberOfAccounts = 0;
    
    //account instance properties
    private String firstName;
    private String lastName;
    private int id;
    private double balance;
    
    public static int getCount()
    {
        return Account.numberOfAccounts;
    }
    
    //constructor
    public Account()
    {
        this.balance = 0;
        numberOfAccounts++;
        this.id= numberOfAccounts;
    }
    
    //constructor
    public Account(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = 0;
        numberOfAccounts++;
        this.id= numberOfAccounts;
    }

    //account methods
    /*getters*/
    public String getFirstName() { return this.firstName; }
    public String getLastName()  { return this.lastName; }
    public int getID()           { return this.id; }
    public double getBalance()   { return this.balance; }
    
    /*setters*/
    public void setFirstName(String first) 
    {
        this.firstName = first;
    }
    
    public void setLastName(String last) 
    {
        this.lastName = last;
    }
    
    
    public String toString()
    {
        return String.format("Account ID: %d\t%s %s\tBalance: $%.2f", 
                             this.id,
                             this.firstName, 
                             this.lastName,
                             this.balance);
    }
    
    //manage assign an id
    
    //deposit money
    public void deposit(double money)
    {
        if (money > 0)
        {
            this.balance += money;
        }
    }
    
    
    //withdraw money
    public void withdraw(double money)
    {
        if (money > 0 && money <= this.balance)
        {
            this.balance -= money;
        }
    }
    
}