public class StudentPoll
{
    public static void main(String[] args)
    {
        /*data setup */
        //data for student responses
        int[] responses = {3,5,1,4,3,4,4,2,3,1,1,1,2,5,4,52};
        
        //array for frequency of scores
        int[] frequency = new int[6];
        
        /*processing */
        //for each response in student scores
        //we want to update the frequency for that score
        for (int score : responses)
        {
            try
            {
                frequency[score]++;
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                System.out.println(e);
            }
        }
        
        //print the results
        for (int i=1; i< frequency.length; i++)
        {
            System.out.printf("%d: %d\n",i, frequency[i]);
        }
    }
}