import java.util.Arrays;

public class ArrayClassMethods
{
    public static void main(String[] args)
    {
        int[] array = {2,4,3,5,1};
        printArray(array);
        
        //Sort
        Arrays.sort(array);
        System.out.println("Sorted array");
        printArray(array);
        //Fill Array
        //Copy Array
        //Equality
        //Search
    }
    
    public static void printArray(int[] arr)
    {
        for (int i : arr)
        {
            System.out.println(i);
        }
    }
}