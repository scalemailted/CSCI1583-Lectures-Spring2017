import java.util.Random;

public class Dice
{
    private static Random randomGenerator = new Random();
    
    public static int rollD6()
    {
        int die = randomGenerator.nextInt(6) + 1;
        return die;
    }
    
    public static int rollD6(int quantity)
    {
        int dieSum = 0;
        for (int i =0; i< quantity; i++)
        {
            int die = randomGenerator.nextInt(6) + 1;
            dieSum += die;
        }
        return dieSum;
    }
    
    public static int rollDice(int... dice)
    {
        int diceSum = 0;
        for (int d : dice)
        {
            int dieValue = randomGenerator.nextInt(d)+1;
            diceSum += dieValue;
        }
        return diceSum;
    }
}