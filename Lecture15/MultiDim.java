public class MultiDim
{
    public static void main(String[] args)
    {
        String[][] names = {
                            {"Ted", "Alex", "Joe"},
                            {"Mary", "Jo", "Lily", "Tim"},
                            {"Mike", "Ed"}
                          };
                          
        //System.out.println(name[1][1]);
        
        for (String[] row : names)
        {
            for (String name : row)
            {
                System.out.println(name);
            }
        }
        
        for(int i=0; i<names.length; i++)
        {
            for (int j=0; j<names[i].length; j++)
            {
                System.out.printf("names[%d][%d] = %s\n",i,j,names[i][j]);
            }
        }
    }
}