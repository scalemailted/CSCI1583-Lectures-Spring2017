public class ItemTester
{
    public static void main(String[] args)
    {
        Item sword = Item.SWORD;
        System.out.println(sword.getDescription());
        System.out.println(sword.getQuantity());
        sword.incrementQuantity();
        System.out.println(sword.getQuantity());
        
    }
}