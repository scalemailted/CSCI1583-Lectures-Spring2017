public class EmployeeTester
{
    public static void main(String[] args)
    {
        Employee jim = new Employee("Jim", "Gimmy", new Date(13,12,1999) );
        System.out.println(jim);
        SalaryEmployee sue = new SalaryEmployee("Sue", "Suzie", new Date(25,1,2009), 100000 );
        System.out.println(sue);
    }
}