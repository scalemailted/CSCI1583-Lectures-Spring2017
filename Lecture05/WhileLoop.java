public class WhileLoop
{
	public static void main(String[] args)
	{
		//loop control variable 
		int product = 3;
		while (product < 100)
		{
			System.out.printf("The product is: %d\n",product);
			product = product * 3;
		}

		System.out.printf("The product is: %d\n",product);
		System.out.printf("The product is: %f\n",1.0/0);
	}
}