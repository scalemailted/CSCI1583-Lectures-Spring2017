
import java.util.Scanner;

public class MultiSelection
{
	public static void main(String[] args)
	{
		/*Data setup*/
		//variable to hold the grade
		double grade;
		//variable to hold scanner object
		Scanner input = new Scanner(System.in);

		/*Process*/
		//prompt the user for a grade
		System.out.print("Enter a numerical grade: ");
		//get the grade from scanner and save to variable
		grade = input.nextDouble();
		//check whether the user passed: 
		if (grade >= 90)
		{
			//print they passed
			System.out.println("You got an A");
		}
		else if (grade >= 80 )
		{
			//print they passed
			System.out.println("You got a B");
		}
		else if (grade >= 70)
		{
			//print they passed
			System.out.println("You got a C");
		}
		else if (grade >= 60)
		{
			//print they passed
			System.out.println("You got a D");
		}
		else 
		{
			//print they passed
			System.out.println("You got a F :(");
		}


		System.out.println("Thank you for using the grade reporter!");
		
	}
}


