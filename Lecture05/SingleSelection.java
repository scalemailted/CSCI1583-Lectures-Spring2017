
import java.util.Scanner;

public class SingleSelection
{
	public static void main(String[] args)
	{
		/*Data setup*/
		//variable to hold the grade
		double grade;
		//variable to hold scanner object
		Scanner input = new Scanner(System.in);

		/*Process*/
		//prompt the user for a grade
		System.out.print("Enter a numerical grade: ");
		//get the grade from scanner and save to variable
		grade = input.nextDouble();
		//check whether the user passed: 
		if (grade >= 60)
		{
			//print they passed
			System.out.println("You passed!");
		}

		System.out.println("Thank you for using the grade reporter!");
		
	}
}


