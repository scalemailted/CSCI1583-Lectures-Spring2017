public class DoWhile
{
    public static void main(String[] args)
    {
        //while loop
        System.out.println("Before the while loop");
        int i = 10;
        while (i<10)
        {
            System.out.printf("%d ", i);
            i++;
        }
        System.out.println("After the while loop");
        
        //do while loop
        System.out.println("Before the do while loop");
        int j = 11;
        do
        {
            System.out.printf("%d\n", j);
            j++;
        }while (j < 10);
        System.out.println("After the do while loop");
        
        //common mistake
        int x = 1;
        while (x<10); //<-- a semicolon after while loop means empty statement just like in for loop example.
        {
            System.out.printf("%d\n",x);
            x++;
        }
    }
}