public class WhileFor
{
    public static void main(String[] args)
    {
        //criteria for counter controlled loop
        //1.) a loop control variable
        //2.) check it for the value we stop at
        //3.) update the loop control variable
        
        //while loop version
        int counter = 0; //1
        while (counter < 10) //2
        {
            //whatever the loop does goes here!
            System.out.printf("%d ",counter);
            counter++; //3
        }
        System.out.println();
        
        //for loop version
        for (int count=0; count < 10; count++)
        {
            //whatever the loop does goes here!
            System.out.printf("%d ",count);
        }
        System.out.println();
        
        //for loop as a while loop
        int count=0;
        for (;count < 10;)
        {
            //whatever the loop does goes here!
            System.out.printf("%d ",count);
            count++;
        }
        System.out.println();
    }
}