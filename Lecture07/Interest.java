//Top: calculate the interest on a savings account
import java.util.Scanner;

public class Interest
{
    public static void main(String[] args)
    {
        /*data setup*/
        Scanner input = new Scanner(System.in);
        double interestRate = 0.05; //interestRate
        double principal;           //principal
        int numberYears;            // numberYears
        
        /*process the data & get a solution*/
        //prompt user for principal
        System.out.print("Enter the starting principal: ");
        //get principal from user and save it
        principal = input.nextDouble();
        
        // prompt user for max number of years
        System.out.print("Enter the number of years to forecast: ");
        // get max number of years from user and save it
        numberYears = input.nextInt();
        
        // repeat for each year until year maxYears 
        for (int year=1;year<=numberYears; year++)
        {
            //amount = principal * (1+interestRate)**numberYears 
            double amount = principal * Math.pow(1+interestRate, year);
            //print the amount
            System.out.printf("%4d%20.2f\n",year,amount);
        }
    }
}