public class CompoundAssignment
{
    public static void main(String[] args)
    {
        int x = 2;
        x *= 10;
        System.out.printf("result: %d\n",x);
        
        //x++;
        System.out.printf("result: %d\n",++x);
        //x--;
        System.out.printf("result: %d\n",x);
        
    }
}