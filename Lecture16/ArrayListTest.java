import java.util.ArrayList;

public class ArrayListTest
{
    public static void main(String[] args)
    {
        ArrayList<String> items = new ArrayList<String>();
        displayArray(items);
        items.add("red");
        displayArray(items);
        items.add( 0,"yellow");
        displayArray(items);
        items.add( 0,"blue");
        displayArray(items);
        items.add( 2,"green");
        
        items.add("yellow");
        displayArray(items);
        
        //System.out.println(items.get(1));
        
        //boolean bool = items.contains("yellow");
        //System.out.printf("That arraylist contains yellow? %b\n",bool);
        
        
        items.remove("yellow");
        //bool = items.contains("yellow");
        //System.out.printf("That arraylist contains yellow? %b\n",bool);
        displayArray(items);
        
    }
        
    
    public static void displayArray(ArrayList<String> items)
    {
        System.out.println("size: "+ items.size() + " " + items);
    }
}