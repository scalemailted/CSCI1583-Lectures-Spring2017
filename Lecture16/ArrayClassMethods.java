import java.util.Arrays;

public class ArrayClassMethods
{
    public static void main(String[] args)
    {
        int[] array = {2,4,3,5,1};
        printArray(array);
        
        //Sort
        Arrays.sort(array);
        System.out.println("Sorted array");
        printArray(array);
        //Fill Array
        int[] fillArray = new int[10];
        Arrays.fill(fillArray, 7);
        System.out.println("Fill array");
        printArray(fillArray);
        
        
        //Copy Array
        int[] arrayCopy = new int[array.length];
        System.arraycopy(array,0,arrayCopy,0,array.length);
        System.out.println("Copy array");
        printArray(arrayCopy);
        
        //Equality
        int[] a = {1,2,4};
        int[] b = {1,2,4};
        boolean bool = a == b;
        System.out.println("array equality");
        System.out.printf("primitive equality: %b\n",bool);
        bool = Arrays.equals(a,b);
        System.out.printf("array equality: %b\n",bool);
        
        
        //Search
        int location = Arrays.binarySearch(array, 6);
        System.out.printf("The location of 3 is in the index of: %d\n",location);
    }
    
    public static void printArray(int[] arr)
    {
        for (int i : arr)
        {
            System.out.println(i);
        }
    }
}